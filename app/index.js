const Generator = require('yeoman-generator');
const commandExists = require('command-exists').sync;

module.exports = class extends Generator {
	constructor(args, opts) {
		super(args, opts);

		// This makes `appname` a required argument.
		this.argument('appname', { type: String, required: false });
		this.argument('skip-install', { type: String, required: false });
	}

	prompting() {
		if (this.options.appname) {
			this.data = {
				name: this.options.appname,
				description: '',
				author: ''
			}

			return
		}

		return this.prompt([
			{
				type: 'input',
				name: 'name',
				message: 'Your project name',
				default: 'app'
			},
			{
				type: 'input',
				name: 'description',
				message: 'Your project description'
			},
			{
				type: 'input',
				name: 'author',
				message: 'Author of the project'
			}
		]).then(x => this.data = x);
	}

	writing() {
		if (!this.data || !this.data.name)
			throw new Error('Appname not provided')

		const files = [
			'__editorconfig',
			'__gitignore',
			'__travis.yml',
			'nodemon.json',
			'readme.md',
			'tsconfig.json',
			'tslint.json',
			'src/index.ts',
			'tests/index.test.ts',
		]

		for (const file of files) {
			this.fs.copyTpl(
				this.templatePath(file),
				this.destinationPath(file.replace('__', '.')),
				this.data
			);
		}

		this.fs.copyTpl(
			this.templatePath('_package.json'),
			this.destinationPath(`package.json`),
			this.data
		);
	}

	install() {
		const hasYarn = commandExists('yarn');
		this.installDependencies({
			npm: !hasYarn,
			bower: false,
			yarn: hasYarn,
			skipMessage: '',
			skipInstall: this.options['skip-install']
		}).then(x => {
			this.log('')
			this.log(`✅ ${this.data.name} - app created successfully!`);
			this.log('')
			this.log(`👉🏻 ${hasYarn ? 'yarn' : 'npm run'} dev`)
			this.log('')
		})
	}
};
