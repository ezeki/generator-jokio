# 🃏 jokio app generator
[![Build Status](https://travis-ci.org/jokio/generator-jokio.svg?branch=master)](https://travis-ci.org/jokio/generator-jokio)
[![engine: jokio](https://img.shields.io/badge/engine-%F0%9F%83%8F%20jokio-44cc11.svg)](https://github.com/jokio/jokio)

jokio app generator for yo

install:
```js
npm i -g yo
npm i -g generator-jokio
```

use:
* `yo jokio` - Wizard
* `yo jokio appname` - Fast generation
